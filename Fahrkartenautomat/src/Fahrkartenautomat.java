﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       // Achtung!!!!
       // Hier wird deutlich, warum double nicht für Geldbeträge geeignet ist.
       // =======================================
       float zuZahlenderBetrag; 
       float eingezahlterGesamtbetrag;
       float eingeworfeneMünze;
       float rückgabebetrag;
       // Die Eingabe erfolgt anwenderfreudlich mit Dezimalpunkt: just testing
       float eingegebenerBetrag;
        // zu 5.: Datentyp ist int, weil Anzahl der Tickets immer ganzzahlig ist
       int anzahlFahrkarten;
       float gesamtpreis;
       
       // Den zu zahlenden Betrag ermittelt normalerweise der Automat
       // aufgrund der gewählten Fahrkarte(n).
       // -----------------------------------
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextFloat();
       System.out.print("Anzahl der Tickets: ");
       anzahlFahrkarten = tastatur.nextInt();
       
       // Bei der Berechnung werden ein float und ein Integer miteinander multipliziert. Der Computer wandelt 
       // den kleineren Datentyp in den größeren um.
       gesamtpreis = anzahlFahrkarten * zuZahlenderBetrag;
       
       // Geldeinwurf 
       // -----------
       eingezahlterGesamtbetrag = 0; 
       while(eingezahlterGesamtbetrag < gesamtpreis)
       {
    	   System.out.printf("%s%.2f%s\n"  , "Noch zu zahlen: " , (gesamtpreis - eingezahlterGesamtbetrag) , " Euro");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nAnzahl ausgegebener Fahrscheine: " + anzahlFahrkarten);
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - gesamtpreis;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("%s%.2f%s\n" , "Der Rückgabebetrag in Höhe von " , rückgabebetrag , " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
    }
}